package org.opendevup.entities;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Enseignant implements Serializable {
	@Id
	@GeneratedValue
	private Long id;
	private String nom;
	private String prenom;
	//private ArrayList<Enseignant> listeEnseignant = new ArrayList<>();
	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Enseignant(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	//	public ArrayList<Enseignant> getListeEnseignant() {
	//		return listeEnseignant;
	//	}
	//	public void setListeEnseignant(ArrayList<Enseignant> listeEnseignant) {
	//		this.listeEnseignant = listeEnseignant;
	//	}
	@Override
	public String toString() {
		return "\n Enseignant [\n nom=" + nom + "\n prenom=" + prenom + "]";
	}

	//	public Enseignant addEnseignant(String nom, String prenom){
	//
	//		Enseignant enseignant = new Enseignant(nom, prenom);
	//		listeEnseignant.add(enseignant);
	//		return enseignant;
	//
	//	}




}
