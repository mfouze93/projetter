package org.opendevup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import javax.swing.plaf.synth.Region;

import org.opendevup.dao.EnseignantRepository;
import org.opendevup.entities.Enseignant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class AEnseignantServiceApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(AEnseignantServiceApplication.class, args);
		EnseignantRepository enseignantRepository = ctx.getBean(EnseignantRepository.class);
		ArrayList<Enseignant> listEnseignants = new ArrayList<>();
		listEnseignants.add(new Enseignant("Ondama", "Fabrice"));
		listEnseignants.add(new Enseignant("Bernard", "Alice"));
		listEnseignants.add(new Enseignant("Macron", "Fabienne"));
		listEnseignants.add(new Enseignant("Marie", "Jean"));
		listEnseignants.add(new Enseignant("Batista", "Vincent"));
		listEnseignants.add(new Enseignant("Costa", "Yann"));
		listEnseignants.add(new Enseignant("Gomez", "Mario"));
		listEnseignants.add(new Enseignant("Hassen", "Phillipe"));
		listEnseignants.add(new Enseignant("Garvin", "Fernando"));
		listEnseignants.add(new Enseignant("Gillardi", "Ilaria"));
		
		
		for(int i=0;i<listEnseignants.size();i++){
			enseignantRepository.save((listEnseignants.get(i)));
			
		}

		enseignantRepository.findAll().forEach(s -> System.out.println(s.toString()));	
		
	}
}
@RestController
class EnseignantRestController {
@RequestMapping("/enseignants")
	Collection<Enseignant> enseignants() {
		return this.enseignantRepository.findAll();
	}
	@Autowired
	EnseignantRepository enseignantRepository;
	
}
