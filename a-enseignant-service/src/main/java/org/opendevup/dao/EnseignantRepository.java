package org.opendevup.dao;

import org.opendevup.entities.Enseignant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface EnseignantRepository extends JpaRepository<Enseignant, Long> {
	
	@Query("SELECT e from Enseignant e where e.nom like :mc")
	public Page<Enseignant> enseignantsParMc(@Param("mc") String mc, Pageable pageable);
	
}
