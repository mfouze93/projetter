package org.opendevup.entities;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Entity
public class Module implements Serializable{
	@Id
	@GeneratedValue
	private Long id;
	private String intitule;
	private String heure;
	private int coef;
	//private ArrayList<Module> listDeModules = new ArrayList<>();

	public Module() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Module(String intitule, String heure, int coef) {
		super();
		this.intitule = intitule;
		this.heure = heure;
		this.coef = coef;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public String getHeure() {
		return heure;
	}
	public void setHeure(String heure) {
		this.heure = heure;
	}
	public int getCoef() {
		return coef;
	}
	public void setCoef(int coef) {
		this.coef = coef;
	}

	//	public ArrayList<Module> getListModules() {
	//		return listModules;
	//	}
	//	public void setListModules(ArrayList<Module> listModules) {
	//		this.listModules = listModules;
	//	}
	//	public Module addModule(String intitule, String heure, int coef){
	//		Module module = new Module(intitule, heure, coef);
	//		listModules.add(module);
	//		return module;
	//	}

	@Override
	public String toString() {
		return "[\n Text:   - Intitule=" + intitule + "\n - heure=" + heure + "\n-coef=" + coef + "]";
	}
	//	
}


