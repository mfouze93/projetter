package org.opendevup.dao;

import org.opendevup.entities.Module;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ModulesRepository extends JpaRepository<Module, Long> {
	
	@Query("SELECT m from Module m where m.intitule like :mc")
	public Page<Module> ModulesParMc(@Param("mc") String mc, Pageable pageable);
	

}
//
//interface CrudRepository<Rep, ID extends Serializable> extends Repository<Module, ID>{
//	 Rep findOne(ID primaryKey);
//	 Iterable<Rep> findAll();
//	 Page<Rep> findAll(Pageable pageable);
//}