package org.opendevup;

import java.util.ArrayList;
import java.util.Collection;

import org.opendevup.dao.ModulesRepository;
import org.opendevup.dao.ModulesRepository;
import org.opendevup.entities.Module;
import org.opendevup.entities.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arjuna.ats.internal.jdbc.drivers.modifiers.list;

@SpringBootApplication
public class AModuleServiceApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(AModuleServiceApplication.class, args);
		
		ModulesRepository ModulesRepository = ctx.getBean(ModulesRepository.class);
		ArrayList<Module> listModules = new ArrayList<>();
		listModules.add(new Module("Conception de base de données", "35H", 5));
		listModules.add(new Module("Tuning Base de données", "35",5));
		listModules.add(new Module("Projet Ter", "120",6));
		listModules.add(new Module("Cryptologie", "35",5));
		listModules.add(new Module("Calculs securisés ", "35",5));
		listModules.add(new Module("Reseaux Etendus", "40H",5));
		listModules.add(new Module("Anglais", "60H",2));
		listModules.add(new Module("Mathematiques", "35H", 5));
		listModules.add(new Module("Gestion de Projets", "40",6));
		listModules.add(new Module("Algorithme Avancé", "50",5));
		
		for(int i=0;i<listModules.size();i++){
			ModulesRepository.save((listModules.get(i)));
			
		}

		
		ModulesRepository.findAll().forEach(s -> System.out.println(s.toString()));
		//System.out.println(ModulesRepository.findOne (1L));
				
	}
}


@RestController
class ModuleRestController {
@RequestMapping("/modules")
	Collection<Module> modules (){
		return this.modulesRepository.findAll();
	}
	@Autowired
	ModulesRepository modulesRepository;
	
}
