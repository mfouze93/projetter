# README #

This README would normally document whatever steps are necessary to get your application up and running.

# Microservices 

Le but de ce projet est de créer une application basée sur les microservices Enseignant et modules dans un premier temps puis un autre service controller qui se charge de la liaison entre ces deux services. 

## Introduction

Les micro services sont une approche d'architecture et de développement d'une application composées de petits services. L'idée étant de découper un grand problème en petites unités implémentée sous forme de micro services. Chaque srevice est responsable d'une fonctionnalité, Chaque micro services est developpé, testé et déployé séparément des autres.Chaque  service tourne dans un processus séparé. La seule relation entre les différents micro services est l'échange de données effectué à travers les différentes APIs qu'ils exposent dans notre cas nous avons utilisé REST

### Prérequis

- Eclipse : 
L'ide utilisé dans le cadre du projet pour la programmation en JAVA
- Eureka
C'est une sorte de registre au coeur de l'application qui permet de localiser les instance de service et ce dans le but de les faire communiquer. La communication entre les parties se fait via les API Web exposées par le composant serveur 

- Spring Boot: 
Framework utilisé durant tout le projet dont les dependances les plus importantes sont:  
```
<dependency>
<groupId>com.h2database</groupId>
<artifactId>h2</artifactId>
<scope>runtime</scope>
</dependency>

<dependency>
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>

<dependency>
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-starter-data-rest</artifactId>
</dependency>

<dependency>
<groupeId>org.springframeworf.cloud</groupeId>
<artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>

```
pour donc le bon fonctionnement du projet  il faut veiller à ce que ces dépendances soient intégrées au fichier pom.xml


### Installation des différentes 

* Spring boot est installé par le biais du marketplace de Eclipse: 

```
1- Ouvrir Eclipse Marketplace
2- Taper Spring dans la barre de recherche
3- Installer Spring Ide
```


## Exécution de l'application
*Exécuter chaque services comme une applicattion java. Les différents ports des applications sont déjà définis. Il s'agit du port **8080** pour le service Enseignant, **8090** pour le service des modules et du port 8000 pour le service controlleur
* Eureka
Pour le fonctionnement de ce service, on a dû le configurer sur le port **8761**. Pour pouvoir permettre l'exécution, on exécute d'abord la classe mère Eureka ensuite dans notre navigateur, tape **localhost:8761 ** et on obtien l'affichage des information sur tous les services connectés.


## Built With

 [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Mafouze Koto Tamou** - *Mise en place des différents services* - [ProjetTer](https://mfouze93@bitbucket.org/mfouze93/projetter.git)
* **Hamza Tahour**
* ** Mohammed Ismaili**

Il y a aussi la liste de tous les participants à voir sur le repository du bitBucket
Par ailleurs, un petit rapport a été ajouté à ce projet pour donner un peu plus d'explication. Pour y accéder veuillez cliquer [ici](https://bitbucket.org/mfouze93/projetter/wiki/Rapport)


## Remerciemmests

* Capacité de programmer de chacun
* Inspiration
* Quiconque utilise le code