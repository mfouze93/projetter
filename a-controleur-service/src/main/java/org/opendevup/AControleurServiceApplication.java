package org.opendevup;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONException;
import org.json.JSONObject;
import org.opendevup.dao.ControlleurRepository;
import org.opendevup.dao.ModulesRepository;
import org.opendevup.entities.Controlleur;
import org.opendevup.entities.Enseignant;
import org.opendevup.entities.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



@SpringBootApplication
public class AControleurServiceApplication {

	public static void main(String[] args) throws JSONException {
		ApplicationContext ctx = SpringApplication.run(AControleurServiceApplication.class, args);
		ControlleurRepository controlleurRepository = ctx.getBean(ControlleurRepository.class);


		
		ArrayList<Controlleur> list = new ArrayList<>();
		list.add(new Controlleur(new Enseignant("Ondama", "Fabrice"),new Module("Conception de base de données", "35H", 5)));
		list.add(new Controlleur(new Enseignant("Bernard", "Alice"),new Module("Tuning Base de données", "35",5)));
		list.add(new Controlleur(new Enseignant("Macron", "Fabienne"),new Module("Projet Ter", "120",6)));
		list.add(new Controlleur(new Enseignant("Marie", "Jean"),new Module("Cryptologie", "35",5)));
		list.add(new Controlleur(new Enseignant("Batista", "Vincent"),new Module("Calculs securisés ", "35",5)));
		list.add(new Controlleur(new Enseignant("Costa", "Yann"),new Module("Reseaux Etendus", "40H",5)));
		list.add(new Controlleur(new Enseignant("Gomez", "Mario"),new Module("Anglais", "60H",2)));
		list.add(new Controlleur(new Enseignant("Hassen", "Phillip"),new Module("Mathematiques", "35H", 5)));
		list.add(new Controlleur(new Enseignant("Garvin", "Fernando"),new Module("Gestion de Projets", "40",6)));
		list.add(new Controlleur(new Enseignant("Gillardi", "Ilaria"),new Module("Algorithme Avancé", "50",5)));



		//Gson gson = new GsonBuilder().setPrettyPrinting().create();
		for(int i=0;i<list.size();i++){
			controlleurRepository.save(new Controlleur (list.get(i).getEnseignant(), list.get(i).getModule()));

		}

		controlleurRepository.findAll().forEach(s -> System.out.println(s.toString()));

	}

}

@RestController
class ControleurRestController {
@RequestMapping("/controleurs")
	Collection<Controlleur> controlleurs (){
		return this.controlleurRepository.findAll();
	}
	@Autowired
	ControlleurRepository controlleurRepository;
	
}
