package org.opendevup.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

 @Entity
public class Controlleur implements Serializable{
	@Id
	@GeneratedValue
	private Long id;
	private Enseignant enseignant;
	private Module module;
	
	public Controlleur(Enseignant enseignant, Module module) {
		super();
		this.enseignant = enseignant;
		this.module = module;
	}
	public Controlleur() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Enseignant getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}
	public Module getModule() {
		return module;
	}
	public void setModule(Module module) {
		this.module = module;
	}
	@Override
	public String toString() {

		//return "\nControlleur [ " + enseignant + "\n " + module + "\n]";
		 return new com.google.gson.Gson().toJson(this);
	} 
	
	
	

}
